[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/VAVI/neuronale-netze-programmieren)

Dieses Projekt bildet die praktischen Aufgaben und Übungen aus dem Buch: "Neuronale Netze programmieren mit Python" nach.

Lernen ist ein aktiver Prozess! Nur Wissen was angewendet wird, bleibt im Gedächtnis haften. 
-Dale Carnegie
